`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: N. Mahen
// 
// Create Date: 03.04.2020 11:36:50
// Design Name: 
// Module Name: SimpleProcessor
// Project Name: Assignment1
// Target Devices: 
// Tool Versions: 
// Description: Brings together all stages for a Simple Processor
// 
// Dependencies: None
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module SimpleProcessor(input clk, input reset);
wire[31:0] Cu_I,readreg1,readreg2,writedata,cdata;
wire Immsel,regwrite,ALUSrc,ALUtoreg;
wire[5:0] opcode,Function;
wire[3:0] ALUop;
InstructionFetch Stage1(clk,reset,Cu_I);
InstructionDecoder Stage2(clk,regwrite,Immsel,Cu_I,writedata,opcode,Function,readreg1,readreg2,cdata);
MainControlUnit MCU(reset,opcode,Function,Immsel,ALUop,ALUtoreg,regwrite,ALUSrc);
ExecutionStg Stage3(readreg1,readreg2,cdata,ALUSrc,ALUtoreg,ALUop,writedata); 
endmodule
