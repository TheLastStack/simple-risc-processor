`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: N. Mahen
// 
// Create Date: 03.04.2020 10:50:05
// Design Name: 
// Module Name: InstructionDecoder
// Project Name: Simple Processor
// Target Devices: 
// Tool Versions: 
// Description: Decodes Instruction and prpares data for execution stage
// 
// Dependencies: None
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module InstructionDecoder(input clk, input regwrite, input Immsel, input[31:0] F_I, input[31:0] writereg, output[5:0] opcode, output[5:0] Function, output[31:0] regread1, output[31:0] regread2, output[31:0] cdata);
assign opcode=F_I[31:26];
assign Function=F_I[5:0];
RegisterMemory RM1(clk, regwrite, F_I[20:16],F_I[15:11],F_I[25:21],writereg,regread1,regread2);
Immgen Im1(Immsel,F_I[20:0],cdata);
endmodule
