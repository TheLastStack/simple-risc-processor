`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: N. Mahen
// 
// Create Date: 03.04.2020 09:50:52
// Design Name: 
// Module Name: MainControlUnit
// Project Name: Assignment 1
// Target Devices: 
// Tool Versions: 
// Description: Generates control signals
// 
// Dependencies: None
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module MainControlUnit(input reset, input[5:0] opcode, input[5:0] Function, output reg Immsel, output[3:0] ALUop, output reg ALUtoreg, output reg regwrite,output ALUSrc);
assign ALUop={Function[5],Function[2],Function[1],Function[0]}|opcode[3:0];
assign ALUSrc=Function[5];
always@(opcode or Function)
begin
    case(opcode)
        6'b111111:begin
                    Immsel <= 0;
                    ALUtoreg <= 0;
                    regwrite <= 1&reset;
                  end
        6'b000000:begin
                    Immsel <= 1;
                    ALUtoreg <= 1;
                    regwrite <= 1&reset;
                  end
        default:begin
                    Immsel <= Immsel;
                    ALUtoreg <= ALUtoreg;
                    regwrite <= 0;
                end
    endcase
end
endmodule
