`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: N. Mahen
// 
// Create Date: 03.04.2020 01:39:35
// Design Name: 
// Module Name: RegisterMemory
// Project Name: Assignment 1
// Target Devices: 
// Tool Versions: 
// Description: Implements register memory.
// 
// Dependencies: None
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module RegisterMemory(input clk, input regwrite, input[4:0] readreg1,input[4:0] readreg2, input[4:0] writereg, input[31:0] writedata, output[31:0] readdata1, output[31:0] readdata2);
reg[31:0] RegMem[31:0];
assign readdata1=RegMem[readreg1];
assign readdata2=RegMem[readreg2];
always@(posedge clk)
begin
    if(regwrite==1'b1)
        RegMem[writereg]=writedata;
    else
        RegMem[writereg]=RegMem[writereg];
end
endmodule
