`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: N. Mahen
// 
// Create Date: 03.04.2020 01:34:30
// Design Name: 
// Module Name: InstructionFetch
// Project Name: Assignment 1
// Target Devices: 
// Tool Versions: 
// Description: Implement's IF stage of a proccessor.
// 
// Dependencies: None
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module InstructionFetch(input clk, input reset, output[31:0] Instruction_Code);
wire[31:0] PC;
PCupdate PC1(PC,clk,reset,PC);
Instruction_Memory IM1(PC, reset,Instruction_Code);
endmodule
