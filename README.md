Simple RISC Processor

Opcode is always `000000`

Function is encoded as follows:

|Operation|Function|
|---------|--------|
|add|100000|
|sub|100010|
|and|100100|
|or|100101|
|sll|000000|
|srl|000010|
