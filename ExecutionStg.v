`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: N. Mahen
// 
// Create Date: 03.04.2020 01:49:08
// Design Name: 
// Module Name: ExecutionStg
// Project Name: Assignment 1
// Target Devices: 
// Tool Versions: 
// Description: Selects and exectues arriving operands
// 
// Dependencies: None
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module ExecutionStg(input[31:0] readdata1,input[31:0] readdata2,input[31:0] cdata, input ALUSrc, input ALUtoreg, input[3:0] ALUop, output reg[31:0] result);
reg[31:0] ALU2;
wire[31:0] ALUresult;
ALU OneALU(readdata1,ALU2,ALUop,ALUresult);
always@(readdata1 or readdata2 or ALUSrc or cdata or ALUresult or ALUtoreg)
begin
    if(ALUSrc==1)
        ALU2 = readdata2;
    else
        ALU2 = cdata;
    if(ALUtoreg==0)
        result = cdata;
    else
        result = ALUresult;
end
endmodule
