`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 07.04.2020 13:43:52
// Design Name: 
// Module Name: PCupdate
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module PCupdate(input[31:0] cPC, input clk, input reset, output reg[31:0] oPC);
reg preset;
always@(posedge clk)
begin
    case({preset,reset})
        2'b11:oPC <= cPC+4;
        2'b01:oPC <= 0;
        2'bx1:oPC <= 0;
        default:oPC <= cPC;
    endcase
    preset=reset;
end
endmodule
