`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: N. Mahen
// 
// Create Date: 03.04.2020 11:16:29
// Design Name: 
// Module Name: Immgen
// Project Name: Assignment 1
// Target Devices: 
// Tool Versions: 
// Description: Converts immediate field to a 32 bit operand. Does sign extension
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module Immgen(input Immsel,input[20:0] inData, output reg[31:0] cdata);
integer i;
always@(Immsel or inData)
begin
case(Immsel)
    1'b0:begin
         for(i=31;i>20;i=i-1)
            begin
            cdata[i] = inData[20];
            end
         cdata[20:0] <= inData;
         end
    1'b1:begin
         for(i=31;i>4;i=i-1)
            begin
            cdata[i] = inData[10];
            end
         cdata[4:0] <= inData[10:6];
         end
endcase
end
endmodule
