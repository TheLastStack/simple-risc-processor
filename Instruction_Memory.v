`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: N. Mahen
// 
// Create Date: 02.04.2020 17:33:21
// Design Name: 
// Module Name: Instruction_Memory
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: Describes a memory containing instructions, represents instruction cache.
// 
// Dependencies: None
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module Instruction_Memory(input[31:0] PC, input reset, output[31:0] Instruction_Code);
reg[7:0] Mem[287:0];
assign Instruction_Code={Mem[PC+3],Mem[PC+2],Mem[PC+1],Mem[PC]};
always@(reset)
begin
if(reset==0)
    begin
    $readmemh("Instructions.mem",Mem);
    end
end
endmodule
